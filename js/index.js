/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var database = null;
var input_aux = null;

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');

        //Deshabilito el zoom de las fonts del celular.
        if (window.MobileAccessibility) {
            window.MobileAccessibility.usePreferredTextZoom(false);
        }

        var container = $('.container');
        //Fix para la apertura del teclado: aumento el height y hago foco en el input.
        window.addEventListener('native.keyboardshow', function (e) {
            container.attr("style", "padding-bottom:" + e.keyboardHeight.toString() + "px");
            scrollToElement("#" + input_aux, 300);
        });


        window.addEventListener('native.keyboardhide', function () {
            container.attr("style", "");
        });

        /*window.addEventListener('keyboardDidShow', function () {
            document.activeElement.scrollIntoView()
        })*/

        //Creaci�n de la DDBB
        var db = window.openDatabase("Database", "1.0", "AppCongreso", 10 * 1024 * 1024);
        db.transaction(crear_tabla_profesionales, errorCB, successCB);
     

    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        //var listeningElement = parentElement.querySelector('.listening');
       // var receivedElement = parentElement.querySelector('.received');

        //listeningElement.setAttribute('style', 'display:none;');
        //receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);

  
    }
};

app.initialize();

// Populate the database
//
function crear_tabla_profesionales(tx) {
    
    tx.executeSql('DROP TABLE IF EXISTS Profesional');
    tx.executeSql('CREATE TABLE IF NOT EXISTS Profesional (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, dni TEXT,trato TEXT,nombre TEXT,apellido TEXT,especialidad INT,matricula_nacional INT,matricula_provincial INT,visitado TEXT,origen_registro TEXT,fecha_alta TEXT)');  
}

function enviar_a_servidor(tx) {
    var myJson = [];
    tx.executeSql('SELECT * FROM Profesional', [], function (tx, results1) {
        for (var i = 0; i < results1.rows.length; i++) {
            myJson.push(results1.rows.item(i));
        }
        $.ajax({
            type: "POST",
            url: "https://www.andromaco.com/DesktopModules/DnnSharp/DnnApiEndpoint/Api.ashx?method=setDatosFromCongresos",
            data: { "datos": myJson }
        }).done(function (datos) {
            alert("POST OK");
        }).fail(function () {
            alert("POST error");
        });
    });
}


function grabar_registro_profesional(tx) {
    var mat_nac = "NULL";
    if ($("#txtMatNacional").val() != "")
    {
        mat_nac = $("#txtMatNacional").val();
    }

    var mat_prov = "NULL";
    if ($("#txtMatProvincial").val() != "") {
        mat_prov = $("#txtMatProvincial").val();
    }

    var d = new Date();
    d = formatDate(d, "dd/MM/yyyy");

    var statement = "INSERT INTO Profesional (email,dni,trato,nombre,apellido,especialidad,matricula_nacional,matricula_provincial,visitado,origen_registro,fecha_alta) VALUES ('" + $("#txtEmail").val() + "', '" + $("#txtDNI").val() + "', '" + $("#ddlTrato").val() + "', '" + $("#txtNombre").val() + "', '" + $("#txtApellido").val() + "', " + $("#ddlEspecialidad").val() + ", " + mat_nac.toString() + ", " + mat_prov.toString() + ", '" + $("#ddlVisitado").val() + "','" + $("#hidCongreso").val()  + "','" + d + "')";
    console.log(statement);
    tx.executeSql(statement);
}



function errorCB(tx, err) {
    console.log("Error en operaci�n SQL: " + err);
}

function successCB() {
    console.log("Operacion SQL Exitosa");
}

function actualizar_en_server()
{
    var db = window.openDatabase("Database", "1.0", "AppCongreso", 10 * 1024 * 1024);
    db.transaction(enviar_a_servidor, errorCB, function () { alert("Se enviaron los datos"); });
}


function campo_valido(obj)
{
    if(obj.val() == "")
    {
        obj.addClass("input-error");
    }
    else
    {
        obj.removeClass("input-error");
    }
}

function vaciar_campos() {
    $("#txtEmail").val("");
    $("#txtDNI").val("");
    $("#txtNombre").val("");
    $("#txtApellido").val("");
    $("#ddlTrato").prop('selectedIndex', 0);
    $("#ddlEspecialidad").prop('selectedIndex', 0);
    $("#ddlVisitado").prop('selectedIndex', 0);
    $("#txtMatNacional").val("");
    $("#txtMatProvincial").val("");
}

var scrollToElement = function (el, ms) {
    var speed = (ms) ? ms : 600;
    $('html,body').animate({
        scrollTop: eval($(el).offset().top - 40)
    }, speed);
}

$("input").click(function () {
    input_aux = $(this).attr("id");
})

function formatDate(date, format, utc) {
    var MMMM = ["\x00", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var MMM = ["\x01", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var dddd = ["\x02", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var ddd = ["\x03", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

    function ii(i, len) {
        var s = i + "";
        len = len || 2;
        while (s.length < len) s = "0" + s;
        return s;
    }

    var y = utc ? date.getUTCFullYear() : date.getFullYear();
    format = format.replace(/(^|[^\\])yyyy+/g, "$1" + y);
    format = format.replace(/(^|[^\\])yy/g, "$1" + y.toString().substr(2, 2));
    format = format.replace(/(^|[^\\])y/g, "$1" + y);

    var M = (utc ? date.getUTCMonth() : date.getMonth()) + 1;
    format = format.replace(/(^|[^\\])MMMM+/g, "$1" + MMMM[0]);
    format = format.replace(/(^|[^\\])MMM/g, "$1" + MMM[0]);
    format = format.replace(/(^|[^\\])MM/g, "$1" + ii(M));
    format = format.replace(/(^|[^\\])M/g, "$1" + M);

    var d = utc ? date.getUTCDate() : date.getDate();
    format = format.replace(/(^|[^\\])dddd+/g, "$1" + dddd[0]);
    format = format.replace(/(^|[^\\])ddd/g, "$1" + ddd[0]);
    format = format.replace(/(^|[^\\])dd/g, "$1" + ii(d));
    format = format.replace(/(^|[^\\])d/g, "$1" + d);

    var H = utc ? date.getUTCHours() : date.getHours();
    format = format.replace(/(^|[^\\])HH+/g, "$1" + ii(H));
    format = format.replace(/(^|[^\\])H/g, "$1" + H);

    var h = H > 12 ? H - 12 : H == 0 ? 12 : H;
    format = format.replace(/(^|[^\\])hh+/g, "$1" + ii(h));
    format = format.replace(/(^|[^\\])h/g, "$1" + h);

    var m = utc ? date.getUTCMinutes() : date.getMinutes();
    format = format.replace(/(^|[^\\])mm+/g, "$1" + ii(m));
    format = format.replace(/(^|[^\\])m/g, "$1" + m);

    var s = utc ? date.getUTCSeconds() : date.getSeconds();
    format = format.replace(/(^|[^\\])ss+/g, "$1" + ii(s));
    format = format.replace(/(^|[^\\])s/g, "$1" + s);

    var f = utc ? date.getUTCMilliseconds() : date.getMilliseconds();
    format = format.replace(/(^|[^\\])fff+/g, "$1" + ii(f, 3));
    f = Math.round(f / 10);
    format = format.replace(/(^|[^\\])ff/g, "$1" + ii(f));
    f = Math.round(f / 10);
    format = format.replace(/(^|[^\\])f/g, "$1" + f);

    var T = H < 12 ? "AM" : "PM";
    format = format.replace(/(^|[^\\])TT+/g, "$1" + T);
    format = format.replace(/(^|[^\\])T/g, "$1" + T.charAt(0));

    var t = T.toLowerCase();
    format = format.replace(/(^|[^\\])tt+/g, "$1" + t);
    format = format.replace(/(^|[^\\])t/g, "$1" + t.charAt(0));

    var tz = -date.getTimezoneOffset();
    var K = utc || !tz ? "Z" : tz > 0 ? "+" : "-";
    if (!utc) {
        tz = Math.abs(tz);
        var tzHrs = Math.floor(tz / 60);
        var tzMin = tz % 60;
        K += ii(tzHrs) + ":" + ii(tzMin);
    }
    format = format.replace(/(^|[^\\])K/g, "$1" + K);

    var day = (utc ? date.getUTCDay() : date.getDay()) + 1;
    format = format.replace(new RegExp(dddd[0], "g"), dddd[day]);
    format = format.replace(new RegExp(ddd[0], "g"), ddd[day]);

    format = format.replace(new RegExp(MMMM[0], "g"), MMMM[M]);
    format = format.replace(new RegExp(MMM[0], "g"), MMM[M]);

    format = format.replace(/\\(.)/g, "$1");

    return format;
};

function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.CELL] = 'Cell generic connection';
    states[Connection.NONE] = 'No network connection';

    alert('Connection type: ' + states[networkState]);
}



//Enviar Informaci�n a LASA 
$(".btnEnviarInfo").click(function () {
    actualizar_en_server();
});
